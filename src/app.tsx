import React from "react"
import { useSelector, useDispatch } from "react-redux"
import { Store } from "./abstractions/store"
import {
  EmployeeActionType,
  GetRequestAction,
} from "./abstractions/store/employee"
import { Auth } from "./realisations/components/auth"
import { Employees } from "./realisations/components/employees"
import { OneEmployee } from "./realisations/components/employees/one"
import "./style.css"

function App() {
  const authState = useSelector((state: Store) => state.auth)
  const dispatch = useDispatch()

  if (!authState.logged) {
    return <Auth dispatch={dispatch} error={authState.error} />
  }

  if (authState.auth?.adminAccess) {
    return (
      <Employees
        init={() => {
          const getAction: GetRequestAction = {
            type: EmployeeActionType.GetRequest,
          }
          dispatch(getAction)
        }}
      ></Employees>
    )
  }

  return (
    <Employees
      init={() => {
        const getAction: GetRequestAction = {
          type: EmployeeActionType.GetRequest,
          filter: { email: authState.auth?.email },
        }
        dispatch(getAction)
      }}
      oneMode={true}
    ></Employees>
  )
}

export default App
