import { EmployeeInterface, Gender } from "."
import {
  BaseRestObjectInterface,
  RestApiHandlerInterface,
} from "../api/handler"

export interface EmployeeRestObject
  extends BaseRestObjectInterface,
    Partial<EmployeeInterface> {
  uuid: string
  firstName: string
  lastName: string
  patronymic: string
  gender: Gender
  contactInfo: string
  salary: number
  position: string
  isWorking?: boolean
  childrenCount: number
  age: number
  experience: number
}

export type EmployeeFilter = {
  firstNameRegEx?: string
  lastNameRegEx?: string
  patronymicRegEx?: string
  positionRegEx?: string
  ageMore?: number
  ageLess?: number
  childrenCountMore?: number
  childrenCountLess?: number
  experienceLess?: number
  experienceMore?: number
  showDismissed?: boolean
  email?: string
}

export interface EmployeeRestApiHandlerInterface
  extends RestApiHandlerInterface<
    EmployeeRestObject,
    EmployeeInterface,
    EmployeeFilter
  > {
  getOneByEmail: (email: string) => Promise<EmployeeInterface>
}
