import { EmployeeInterface } from "../employees"
import { EmployeeFilter } from "../employees/handler"

export enum EmployeeActionType {
  StartCreating = "EMPLOYEE_START_CREATING",
  CancelCreating = "EMPLOYEE_CANCEL_CREATING",
  CreateRequest = "EMPLOYEE_CREATE_REQUEST",
  CreateSuccess = "EMPLOYEE_CREATE_SUCCESS",
  CreateError = "EMPLOYEE_CREATE_ERROR",
  GetRequest = "EMPLOYEE_GET_REQUEST",
  GetSuccess = "EMPLOYEE_GET_SUCCESS",
  GetError = "EMPLOYEE_GET_ERROR",
  UpdateRequest = "EMPLOYEE_UPDATE_REQUEST",
  UpdateSuccess = "EMPLOYEE_UPDATE_SUCCESS",
  UpdateError = "EMPLOYEE_UPDATE_ERROR",
  DeleteRequest = "EMPLOYEE_DELETE_REQUEST",
  DeleteSuccess = "EMPLOYEE_DELETE_SUCCESS",
  DeleteError = "EMPLOYEE_DELETE_ERROR",
  GetByEmailRequest = "EMPLOYEE_GET_BY_EMAIL",
}

//create

export type StartCreatingAction = {
  type: EmployeeActionType.StartCreating
}

export type CancelCreatingAction = {
  type: EmployeeActionType.CancelCreating
  uuid: string
}

export type CreateRequestAction = {
  type: EmployeeActionType.CreateRequest
  element: EmployeeInterface
}

export type CreateSuccessAction = {
  type: EmployeeActionType.CreateSuccess
  element: EmployeeInterface
  elementUuid: string
}
export type CreateErrorAction = {
  type: EmployeeActionType.CreateError
  element: EmployeeInterface
  error: string
}

type CreateAction =
  | StartCreatingAction
  | CancelCreatingAction
  | CreateRequestAction
  | CreateSuccessAction
  | CreateErrorAction

//get

export type GetRequestAction = {
  type: EmployeeActionType.GetRequest
  filter?: EmployeeFilter
}

export type GetSuccessAction = {
  type: EmployeeActionType.GetSuccess
  elements: EmployeeInterface[]
}

export type GetErrorAction = {
  type: EmployeeActionType.GetError
  error: string
}

type GetAction = GetRequestAction | GetSuccessAction | GetErrorAction

//update

export type UpdateRequestAction = {
  type: EmployeeActionType.UpdateRequest
  element: Partial<EmployeeInterface>
  uuid: string
}

export type UpdateSuccessAction = {
  type: EmployeeActionType.UpdateSuccess
  elementUpdate: Partial<EmployeeInterface>
  uuid: string
}

export type UpdateErrorAction = {
  type: EmployeeActionType.UpdateError
  error: string
}

type UpdateAction = UpdateSuccessAction

//delete

export type DeleteRequestAction = {
  type: EmployeeActionType.DeleteRequest
  uuid: string
}

export type DeleteSuccessAction = {
  type: EmployeeActionType.DeleteSuccess
  uuid: string
}

export type DeleteErrorAction = {
  type: EmployeeActionType.DeleteError
  error: string
  uuid: string
}

type DeleteAction =
  | DeleteRequestAction
  | DeleteSuccessAction
  | DeleteErrorAction

export type EmployeeAction =
  | CreateAction
  | GetAction
  | UpdateAction
  | DeleteAction
