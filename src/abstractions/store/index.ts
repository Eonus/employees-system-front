import { AuthBaseInterface } from "../api/auth"
import { EmployeeInterface } from "../employees"

export type AuthState = {
  logged: boolean
  auth?: AuthBaseInterface
  error?: string
}

export type EmployeeState = {
  elements: EmployeeInterface[]
}

export type Store = {
  auth: AuthState
  employees: EmployeeState
}
