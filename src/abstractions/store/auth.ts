export enum AuthActionType {
  LoginRequest = "LOGIN_REQUEST",
  LoginSuccess = "LOGIN_SUCCESS",
  LoginError = "LOGIN_ERROR",
  RegisterRequest = "REGISTER_REQUEST",
  RegisterSuccess = "REGISTER_SUCCESS",
  RegisterError = "REGISTER_ERROR",
}

//login

export type LoginRequestAction = {
  type: AuthActionType.LoginRequest
  login: string
  password: string
}

export type LoginSuccessAction = {
  type: AuthActionType.LoginSuccess
  login: string
  email: string
  password: string
  adminAccess?: boolean
}

export type LoginErrorAction = {
  type: AuthActionType.LoginError
  error: string
}

//register

export type RegisterRequestAction = {
  type: AuthActionType.RegisterRequest
  login: string
  password: string
  email: string
}

export type RegisterSuccessAction = {
  type: AuthActionType.RegisterSuccess
  login: string
  password: string
  email: string
}

export type RegisterErrorAction = {
  type: AuthActionType.RegisterError
  error: string
}

export type AuthAction =
  | LoginRequestAction
  | LoginSuccessAction
  | LoginErrorAction
  | RegisterRequestAction
  | RegisterSuccessAction
  | RegisterErrorAction
