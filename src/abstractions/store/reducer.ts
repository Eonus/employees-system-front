import { AuthActionType } from "./auth"
import { EmployeeActionType } from "./employee"

export type ActionType = AuthActionType | EmployeeActionType // || another  action type

export interface BaseActionInterface {
  type: ActionType
}

export interface ReducerInterface<State, Action extends BaseActionInterface> {
  reduce(state: State, action: Action): State
}
