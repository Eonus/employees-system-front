export interface BaseRestObjectInterface {
  uuid: string
}

export interface RestApiHandlerInterface<
  RestRequestObject extends BaseRestObjectInterface,
  RestResponseObject extends RestRequestObject,
  Filter
> {
  getOne: (uuid: string) => Promise<RestResponseObject>
  getMany: (filter?: Filter) => Promise<RestResponseObject[]>
  post: (object: Omit<RestRequestObject, "uuid">) => Promise<string>
  put: (uuid: string, object: Partial<RestRequestObject>) => Promise<void>
  delete: (uuid: string) => Promise<void>
}
