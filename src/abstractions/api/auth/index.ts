export interface AuthBaseInterface {
  login: string
  password: string
  email: string
  adminAccess?: boolean
}

export interface AuthProviderInterface<
  AuthInterface extends AuthBaseInterface
> {
  register: (info: AuthInterface) => Promise<void>
  login: (info: AuthBaseInterface) => Promise<{ user: AuthInterface }>
}
