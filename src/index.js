import React from "react"
import ReactDOM from "react-dom"
import App from "./app"
import { Provider } from "react-redux"
import { AuthProvider } from "./realisations/api/auth"
import { SagaCounter } from "./sagas"
import createSagaMiddleware from "redux-saga"
import { applyMiddleware, createStore } from "@reduxjs/toolkit"
import { rootReducer } from "./realisations/store/reducers/root"
import { ApiCaller } from "./realisations/api"
import { EmployeeHandler } from "./realisations/employees/handler"

if (!process.env.REACT_APP_API_URL) {
  throw Error("REACT_APP_API_URL was not provided in .env")
}
const env = {
  API_URL: process.env.REACT_APP_API_URL,
}
console.log(env)

const apiCaller = new ApiCaller()

const authProvider = new AuthProvider(
  env.API_URL + "login",
  env.API_URL + "register",
  apiCaller,
)

const employeeHandler = new EmployeeHandler(env.API_URL + "employee", apiCaller)

const sagaCounter = new SagaCounter(authProvider, employeeHandler)
const sagaMiddleware = createSagaMiddleware()
const store = createStore(rootReducer, applyMiddleware(sagaMiddleware))
sagaMiddleware.run(SagaCounter.rootSaga)

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <App />
    </Provider>
  </React.StrictMode>,
  document.getElementById("root"),
)
