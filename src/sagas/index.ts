import { all, call, put, takeEvery } from "redux-saga/effects"
import { EmployeeRestApiHandlerInterface } from "../abstractions/employees/handler"
import {
  AuthActionType,
  LoginErrorAction,
  LoginRequestAction,
  LoginSuccessAction,
  RegisterRequestAction,
  RegisterSuccessAction,
} from "../abstractions/store/auth"
import {
  CreateErrorAction,
  CreateRequestAction,
  CreateSuccessAction,
  DeleteErrorAction,
  DeleteRequestAction,
  DeleteSuccessAction,
  EmployeeActionType,
  GetErrorAction,
  GetRequestAction,
  GetSuccessAction,
  UpdateErrorAction,
  UpdateRequestAction,
  UpdateSuccessAction,
} from "../abstractions/store/employee"
import { AuthProvider } from "../realisations/api/auth"

export class SagaCounter {
  private static authProvider: AuthProvider
  private static employeeHandler: EmployeeRestApiHandlerInterface
  constructor(
    authProvider: AuthProvider,
    employeeHandler: EmployeeRestApiHandlerInterface,
  ) {
    SagaCounter.authProvider = authProvider
    SagaCounter.employeeHandler = employeeHandler
  }

  private static * tryLogin(action: LoginRequestAction) {
    try {
      //@ts-ignore
      const result = yield call(SagaCounter.authProvider.login, action)
      const successAction: LoginSuccessAction = {
        type: AuthActionType.LoginSuccess,
        login: action.login,
        password: action.password,
        adminAccess: result.user.adminAccess,
        email: result.user.email,
      }
      yield put(successAction)
    } catch (e) {
      const errorAction: LoginErrorAction = {
        type: AuthActionType.LoginError,
        error: e.message,
      }
      yield put(errorAction)
    }
  }

  private static * watchTryLogin() {
    yield takeEvery(AuthActionType.LoginRequest, SagaCounter.tryLogin)
  }

  private static * tryRegister(action: RegisterRequestAction) {
    try {
      yield call(SagaCounter.authProvider.register, action)
      const successAction: RegisterSuccessAction = {
        type: AuthActionType.RegisterSuccess,
        login: action.login,
        password: action.password,
        email: action.email,
      }
      yield put(successAction)
    } catch (e) {
      const errorAction: LoginErrorAction = {
        type: AuthActionType.LoginError,
        error: e.message,
      }
      yield put(errorAction)
    }
  }

  private static * watchTryRegister() {
    yield takeEvery(AuthActionType.RegisterRequest, SagaCounter.tryRegister)
  }

  private static * getEmployees(action: GetRequestAction) {
    try {
      let elements
      if (!action.filter?.email) {
        //@ts-ignore
        elements = yield call(
          SagaCounter.employeeHandler.getMany,
          action.filter,
        )
      } else {
        //@ts-ignore
        elements = yield call(
          SagaCounter.employeeHandler.getOneByEmail,
          action.filter.email,
        )
        elements = [elements]
      }
      const successAction: GetSuccessAction = {
        type: EmployeeActionType.GetSuccess,
        elements,
      }
      yield put(successAction)
    } catch (e) {
      const errorAction: GetErrorAction = {
        type: EmployeeActionType.GetError,
        error: e.message,
      }
      yield put(errorAction)
    }
  }

  private static * watchGetEmployees() {
    yield takeEvery(EmployeeActionType.GetRequest, SagaCounter.getEmployees)
  }

  private static * deleteEmployee(action: DeleteRequestAction) {
    try {
      yield call(SagaCounter.employeeHandler.delete, action.uuid)
      const successAction: DeleteSuccessAction = {
        type: EmployeeActionType.DeleteSuccess,
        uuid: action.uuid,
      }
      yield put(successAction)
    } catch (e) {
      const errorAction: DeleteErrorAction = {
        type: EmployeeActionType.DeleteError,
        error: e.message,
        uuid: action.uuid,
      }
      yield put(errorAction)
    }
  }

  private static * watchDeleteEmployee() {
    yield takeEvery(
      EmployeeActionType.DeleteRequest,
      SagaCounter.deleteEmployee,
    )
  }

  private static * createEmployee(action: CreateRequestAction) {
    try {
      //@ts-ignore
      const objectId = yield call(
        SagaCounter.employeeHandler.post,
        action.element,
      )
      const successAction: CreateSuccessAction = {
        type: EmployeeActionType.CreateSuccess,
        element: action.element,
        elementUuid: objectId,
      }
      yield put(successAction)
    } catch (e) {
      const errorAction: CreateErrorAction = {
        type: EmployeeActionType.CreateError,
        error: e.message,
        element: action.element,
      }
      yield put(errorAction)
    }
  }

  private static * watchCreateEmployee() {
    yield takeEvery(
      EmployeeActionType.CreateRequest,
      SagaCounter.createEmployee,
    )
  }

  private static * updateEmployee(action: UpdateRequestAction) {
    try {
      yield call(SagaCounter.employeeHandler.put, action.uuid, action.element)
      const successAction: UpdateSuccessAction = {
        type: EmployeeActionType.UpdateSuccess,
        elementUpdate: action.element,
        uuid: action.uuid,
      }
      yield put(successAction)
    } catch (e) {
      const errorAction: UpdateErrorAction = {
        type: EmployeeActionType.UpdateError,
        error: e.message,
      }
      yield put(errorAction)
    }
  }

  private static * watchUpdateEmployee() {
    yield takeEvery(
      EmployeeActionType.UpdateRequest,
      SagaCounter.updateEmployee,
    )
  }

  static * rootSaga() {
    yield all([
      SagaCounter.watchTryLogin(),
      SagaCounter.watchTryRegister(),
      SagaCounter.watchGetEmployees(),
      SagaCounter.watchDeleteEmployee(),
      SagaCounter.watchCreateEmployee(),
      SagaCounter.watchUpdateEmployee(),
    ])
  }
}
