import React from "react"
import {
  AuthAction,
  AuthActionType,
  LoginRequestAction,
  RegisterRequestAction,
} from "../../../abstractions/store/auth"

export class Auth extends React.Component<
  {
    dispatch: (action: AuthAction) => void
    error?: string
  },
  { authMode?: boolean }
> {
  private loginId = "login"
  private passwordId = "password"
  private emailId = "email"
  state = {
    authMode: true,
  }

  private onClick = () => {
    if (this.state.authMode) this.onLogin()
    else this.onRegister()
  }

  onLogin = () => {
    const login = (document.getElementById(this.loginId) as HTMLInputElement)
      .value
    const password = (document.getElementById(
      this.passwordId,
    ) as HTMLInputElement).value
    const action: LoginRequestAction = {
      type: AuthActionType.LoginRequest,
      login,
      password,
    }
    this.props.dispatch(action)
  }

  onRegister = () => {
    const login = (document.getElementById(this.loginId) as HTMLInputElement)
      .value
    const password = (document.getElementById(
      this.passwordId,
    ) as HTMLInputElement).value
    const email = (document.getElementById(this.emailId) as HTMLInputElement)
      .value
    const action: RegisterRequestAction = {
      type: AuthActionType.RegisterRequest,
      login,
      password,
      email,
    }
    this.props.dispatch(action)
  }

  getError = () => {
    return <div>{this.props.error}</div>
  }

  changeMode = () => {
    this.setState({
      ...this.state,
      authMode: !this.state.authMode,
    })
  }

  getBottom = () => {
    if (this.state.authMode) {
      return (
        <div>
          <button onClick={() => this.onClick()}>Login</button>
          <br />
          <button onClick={() => this.changeMode()}>I want register</button>
        </div>
      )
    } else {
      return (
        <div>
          <input type="text" id={this.emailId} placeholder="email"></input>
          <br />
          <button onClick={() => this.onClick()}>Register</button>
          <br />
          <button onClick={() => this.changeMode()}>I want login</button>
        </div>
      )
    }
  }

  render = () => {
    return (
      <div>
        <input type="text" id={this.loginId} placeholder="login"></input>
        <br />
        <input
          type="password"
          id={this.passwordId}
          placeholder="password"
        ></input>
        <br />
        {this.getBottom()}
        {this.getError()}
      </div>
    )
  }
}
