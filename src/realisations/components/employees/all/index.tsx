import React, { Dispatch } from "react"
import { useDispatch } from "react-redux"
import { EmployeeInterface } from "../../../../abstractions/employees"
import { EmployeeFilter } from "../../../../abstractions/employees/handler"
import {
  CancelCreatingAction,
  CreateRequestAction,
  DeleteRequestAction,
  EmployeeActionType,
  GetRequestAction,
  StartCreatingAction,
  UpdateRequestAction,
} from "../../../../abstractions/store/employee"
import { Filter } from "../filter"
import { OneEmployee } from "../one"

let dispatch: Dispatch<any>

const deleteEmployee = (uuid: string) => {
  const deleteAction: DeleteRequestAction = {
    type: EmployeeActionType.DeleteRequest,
    uuid,
  }
  dispatch(deleteAction)
}

const createEmployee = (element: EmployeeInterface) => {
  const createAction: CreateRequestAction = {
    type: EmployeeActionType.CreateRequest,
    element,
  }
  dispatch(createAction)
}

const updateEmployee = (element: EmployeeInterface) => {
  const updateAction: UpdateRequestAction = {
    type: EmployeeActionType.UpdateRequest,
    element,
    uuid: element.uuid,
  }
  dispatch(updateAction)
}

const startCreating = () => {
  const startCreatingAction: StartCreatingAction = {
    type: EmployeeActionType.StartCreating,
  }
  dispatch(startCreatingAction)
}

const cancelCreating = (uuid: string) => {
  const cancelCreatingAction: CancelCreatingAction = {
    type: EmployeeActionType.CancelCreating,
    uuid,
  }
  dispatch(cancelCreatingAction)
}

const getOneEmployee = (element: EmployeeInterface, oneMode?: boolean) => {
  return (
    <OneEmployee
      key={element.uuid}
      element={element}
      delete={deleteEmployee}
      finishCreating={createEmployee}
      cancelCreating={cancelCreating}
      update={updateEmployee}
      oneMode={oneMode}
    ></OneEmployee>
  )
}

const getAllEmployees = (elements: EmployeeInterface[], oneMode?: boolean) => {
  const result = []
  for (let i = 0; i < elements.length; i++) {
    result.push(getOneEmployee(elements[i], oneMode))
  }

  return result
}

const getWithFilter = (filter: EmployeeFilter) => {
  const getAction: GetRequestAction = {
    type: EmployeeActionType.GetRequest,
    filter,
  }
  dispatch(getAction)
  console.log(filter)
}

export const AllEmployees = (props: {
  elements: EmployeeInterface[]
  oneMode?: boolean
}) => {
  dispatch = useDispatch()
  if (props.oneMode) {
    if (!props.elements.length) {
      return <h2>You are not employee</h2>
    }
    return (
      <div>
        {" "}
        <h2>Your information:</h2>
        <div id="title">
          <h4>First name</h4>
          <h4>Last name</h4>
          <h4>Patroymic</h4>
          <h4 style={{ marginLeft: 10 }}>Gender</h4>
          <h4>Position</h4>
          <h4>Contact</h4>
          <h4 style={{ marginLeft: 10 }}>Salary</h4>
          <h4 style={{ marginLeft: 10 }}>Age</h4>
          <h4>Experience</h4>
          <h4>Children count</h4>
        </div>
        <div>{getAllEmployees(props.elements, props.oneMode)}</div>
      </div>
    )
  }
  return (
    <div>
      <Filter getWithFilter={getWithFilter}></Filter>
      <div id="title">
        <h4>First name</h4>
        <h4>Last name</h4>
        <h4>Patroymic</h4>
        <h4>Gender</h4>
        <h4>Position</h4>
        <h4>Contact</h4>
        <h4>Salary</h4>
        <h4>Age</h4>
        <h4>Experience</h4>
        <h4>Children count</h4>
        <h4>Manage</h4>
      </div>
      <div>{getAllEmployees(props.elements)}</div>
      <div id="create">
        <button onClick={startCreating} disabled={props.elements[0].dismissed}>
          Create
        </button>
      </div>
    </div>
  )
}
