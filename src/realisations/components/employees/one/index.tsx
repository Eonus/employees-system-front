import React from "react"
import { EmployeeInterface, Gender } from "../../../../abstractions/employees"

export class OneEmployee extends React.Component<
  {
    element: EmployeeInterface
    delete: (uuid: string) => void
    finishCreating: (element: EmployeeInterface) => void
    cancelCreating: (uuid: string) => void
    update: (element: EmployeeInterface) => void
    defaultEditing?: boolean
    isCreating?: boolean
    oneMode?: boolean
  },
  { editing: boolean }
> {
  state = {
    editing: this.props.defaultEditing || false,
    creating: this.props.isCreating || this.props.element.isCreating || false,
  }

  ids = {
    firstName: this.props.element.uuid + "-first-name",
    lastName: this.props.element.uuid + "-last-name",
    patronymic: this.props.element.uuid + "-patronymic",
    gender: this.props.element.uuid + "-gender",
    contactInfo: this.props.element.uuid + "-contact-info",
    salary: this.props.element.uuid + "-salary",
    position: this.props.element.uuid + "-position",
    childrenCount: this.props.element.uuid + "-children-count",
    age: this.props.element.uuid + "-age",
    experience: this.props.element.uuid + "-experience",
  }

  getData = (): EmployeeInterface | null => {
    const firstName = (document.getElementById(
      this.ids.firstName,
    ) as HTMLInputElement).value
    const lastName = (document.getElementById(
      this.ids.lastName,
    ) as HTMLInputElement).value
    const patronymic = (document.getElementById(
      this.ids.patronymic,
    ) as HTMLInputElement).value
    const genderString = (document.getElementById(
      this.ids.gender,
    ) as HTMLInputElement).value
    let gender
    if (genderString === Gender.Male || genderString === "мужской") {
      gender = Gender.Male
    } else if (genderString === Gender.Female || genderString === "женский") {
      gender = Gender.Female
    } else {
      alert("Gender can be `male` or `female` only")
      return null
    }
    const contactInfo = (document.getElementById(
      this.ids.contactInfo,
    ) as HTMLInputElement).value
    const salaryString = (document.getElementById(
      this.ids.salary,
    ) as HTMLInputElement).value
    let salary
    try {
      salary = parseInt(salaryString)
    } catch (e) {
      alert("Salary can be number only")
      return null
    }
    const position = (document.getElementById(
      this.ids.position,
    ) as HTMLInputElement).value
    const childrenCountString = (document.getElementById(
      this.ids.childrenCount,
    ) as HTMLInputElement).value
    let childrenCount
    try {
      childrenCount = parseInt(childrenCountString)
    } catch (error) {
      alert("Children count can be number only")
      return null
    }
    const ageString = (document.getElementById(
      this.ids.age,
    ) as HTMLInputElement).value
    let age
    try {
      age = parseInt(ageString)
    } catch (error) {
      alert("Age can be number only")
      return null
    }
    const experienceString = (document.getElementById(
      this.ids.experience,
    ) as HTMLInputElement).value
    let experience
    try {
      experience = parseInt(experienceString)
    } catch (error) {
      alert("Experience can be number only")
      return null
    }
    return {
      uuid: this.props.element.uuid,
      firstName,
      lastName,
      patronymic,
      gender,
      contactInfo,
      salary,
      position,
      childrenCount,
      age,
      experience,
    }
  }

  onCreate = () => {
    const data = this.getData()
    console.log(data)
    if (!data) return
    this.props.finishCreating(data)
  }

  onCreatingCancel = () => {
    this.props.cancelCreating(this.props.element.uuid)
  }

  getCreateButton = () => {
    return (
      <div>
        <button onClick={this.onCreate}>Create</button>
        <button onClick={this.onCreatingCancel}>Cancel</button>
      </div>
    )
  }

  onUpdateDone = () => {
    const data = this.getData()
    if (!data) return
    this.props.update(data)
    this.setState({
      ...this.state,
      editing: false,
    })
  }

  onUpdatingCancel = () => {
    this.setState({
      ...this.state,
      editing: false,
    })
  }

  getButton = () => {
    if (this.state.creating) return this.getCreateButton()
    return (
      <div>
        <button onClick={this.onUpdateDone}>Done</button>
        <button onClick={this.onUpdatingCancel}>Cancel</button>
      </div>
    )
  }

  getEditing = () => {
    return (
      <div className="employee">
        <input
          type="text"
          id={this.ids.firstName}
          defaultValue={this.props.element.firstName}
          placeholder="first name"
          title="first name"
        ></input>
        <input
          type="text"
          id={this.ids.lastName}
          defaultValue={this.props.element.lastName}
          placeholder="last name"
          title="last name"
        ></input>
        <input
          type="text"
          id={this.ids.patronymic}
          defaultValue={this.props.element.patronymic}
          placeholder="patronymic"
          title="patronymic"
        ></input>
        <input
          type="text"
          id={this.ids.gender}
          defaultValue={this.props.element.gender}
          placeholder="gender"
          title="gender"
        ></input>
        <input
          type="text"
          id={this.ids.position}
          defaultValue={this.props.element.position}
          placeholder="position"
          title="position"
        ></input>
        <input
          type="text"
          id={this.ids.contactInfo}
          defaultValue={this.props.element.contactInfo}
          placeholder="contact info"
          title="last name"
        ></input>
        <input
          type="text"
          id={this.ids.salary}
          defaultValue={this.props.element.salary}
          placeholder="salary"
          title="salary"
        ></input>
        <input
          type="text"
          id={this.ids.age}
          defaultValue={this.props.element.age}
          placeholder="age"
          title="age"
        ></input>
        <input
          type="text"
          id={this.ids.experience}
          defaultValue={this.props.element.experience}
          placeholder="experience"
          title="experience"
        ></input>
        <input
          type="text"
          id={this.ids.childrenCount}
          defaultValue={this.props.element.childrenCount}
          placeholder="children count"
          title="children count"
        ></input>
        {this.getButton()}
      </div>
    )
  }

  getDismissButton = () => {
    return (
      <button
        onClick={() => {
          this.props.delete(this.props.element.uuid)
        }}
        disabled={this.props.element.dismissed}
      >
        Dismiss
      </button>
    )
  }

  getRightButtons = () => {
    if (this.props.oneMode) return
    return (
      <div>
        <button
          onClick={() => {
            this.setState({ ...this.state, editing: true })
          }}
          disabled={this.props.element.dismissed}
        >
          Edit
        </button>
        {this.getDismissButton()}
      </div>
    )
  }

  render = () => {
    if (this.state.editing || this.state.creating) return this.getEditing()

    return (
      <div className="employee">
        <div className="one-info" title="first name">
          {this.props.element.firstName}
        </div>
        <div className="one-info" title="last name">
          {this.props.element.lastName}
        </div>
        <div className="one-info" title="patronymic">
          {this.props.element.patronymic}
        </div>
        <div className="one-info" title="gender">
          {this.props.element.gender === Gender.Male ? "мужской" : "женский"}
        </div>
        <div className="one-info" title="position">
          {this.props.element.position}
        </div>
        <div className="one-info" title="contact info">
          {this.props.element.contactInfo}
        </div>
        <div className="one-info" title="salary">
          {this.props.element.salary}
        </div>
        <div className="one-info" title="age">
          {this.props.element.age}
        </div>
        <div className="one-info" title="experience">
          {this.props.element.experience}
        </div>
        <div className="one-info" title="children count">
          {this.props.element.childrenCount}
        </div>
        {this.getRightButtons()}
      </div>
    )
  }
}
