import React, { useEffect } from "react"
import { useSelector } from "react-redux"
import { Store } from "../../../abstractions/store"
import { AllEmployees } from "./all"

export const Employees = (props: { init: () => void; oneMode?: boolean }) => {
  useEffect(() => {
    props.init()
  }, [])
  const data = useSelector((state: Store) => state.employees)
  return (
    <div>
      <AllEmployees
        elements={data.elements}
        oneMode={props.oneMode}
      ></AllEmployees>
    </div>
  )
}
