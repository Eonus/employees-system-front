import React from "react"
import { EmployeeFilter } from "../../../../abstractions/employees/handler"

const activeFilter: EmployeeFilter = {}

let getWithFilter: (filter: EmployeeFilter) => void

const ids = {
  firstName: "first-name-filter",
  lastName: "last-name-filter",
  patronymic: "patronymic-filter",
  position: "position-filter",
  ageMore: "age-more-filter",
  ageLess: "age-less-filter",
  experienceMore: "experience-more-filter",
  experienceLess: "experience-less-filter",
  childrenCountMore: "children-count-more-filter",
  childrenCountLess: "children-count-less-filter",
  showDismissed: "dismissed-filter",
}

const onFirstNameChange = () => {
  const firstName = (document.getElementById(ids.firstName) as HTMLInputElement)
    .value
  activeFilter.firstNameRegEx = "^" + firstName
  getWithFilter(activeFilter)
}

const onLastNameChange = () => {
  const lastName = (document.getElementById(ids.lastName) as HTMLInputElement)
    .value
  activeFilter.lastNameRegEx = "^" + lastName
  getWithFilter(activeFilter)
}

const onPatronymicChange = () => {
  const patronymic = (document.getElementById(
    ids.patronymic,
  ) as HTMLInputElement).value
  activeFilter.patronymicRegEx = "^" + patronymic
  getWithFilter(activeFilter)
}

const onPositionChange = () => {
  const position = (document.getElementById(ids.position) as HTMLInputElement)
    .value
  activeFilter.positionRegEx = "^" + position
  getWithFilter(activeFilter)
}

const onAgeChange = () => {
  const ageMoreSting = (document.getElementById(
    ids.ageMore,
  ) as HTMLInputElement).value
  let ageMore
  try {
    ageMore = parseInt(ageMoreSting) || undefined
  } catch (e) {
    alert("AgeMore can be only number")
    return
  }
  const ageLessString = (document.getElementById(
    ids.ageLess,
  ) as HTMLInputElement).value
  let ageLess
  try {
    ageLess = parseInt(ageLessString) || undefined
  } catch (e) {
    alert("AgeLess can be only number")
    return
  }
  activeFilter.ageMore = ageMore
  activeFilter.ageLess = ageLess
  getWithFilter(activeFilter)
}

const onExperienceChange = () => {
  const experienceMoreSting = (document.getElementById(
    ids.experienceMore,
  ) as HTMLInputElement).value
  let experienceMore
  try {
    experienceMore = parseInt(experienceMoreSting) || undefined
  } catch (e) {
    alert("ExperienceMore can be only number")
    return
  }
  const experienceLessString = (document.getElementById(
    ids.experienceLess,
  ) as HTMLInputElement).value
  let experienceLess
  try {
    experienceLess = parseInt(experienceLessString) || undefined
  } catch (e) {
    alert("ExperienceLess can be only number")
    return
  }
  activeFilter.experienceMore = experienceMore
  activeFilter.experienceLess = experienceLess
  getWithFilter(activeFilter)
}

const onChildrenCountChange = () => {
  const childrenCountMoreSting = (document.getElementById(
    ids.childrenCountMore,
  ) as HTMLInputElement).value
  let childrenCountMore
  try {
    childrenCountMore = parseInt(childrenCountMoreSting) || undefined
  } catch (e) {
    alert("ChildrenCountMore can be only number")
    return
  }
  const childrenCountLessString = (document.getElementById(
    ids.childrenCountLess,
  ) as HTMLInputElement).value
  let childrenCountLess
  try {
    childrenCountLess = parseInt(childrenCountLessString) || undefined
  } catch (e) {
    alert("ChildrenCountLess can be only number")
    return
  }
  activeFilter.childrenCountMore = childrenCountMore
  activeFilter.childrenCountLess = childrenCountLess
  getWithFilter(activeFilter)
}

const onDismissedChange = () => {
  const value = (document.getElementById(ids.showDismissed) as HTMLInputElement)
    .checked
  activeFilter.showDismissed = value
  getWithFilter(activeFilter)
}

export const Filter = (props: {
  getWithFilter: (filter: EmployeeFilter) => void
}) => {
  getWithFilter = props.getWithFilter
  return (
    <div
      style={{
        display: "flex",
        flexDirection: "column",
        width: "200px",
        float: "left",
      }}
    >
      <input
        type="text"
        id={ids.firstName}
        placeholder="first name filter"
        title="first name filter"
        onChange={() => onFirstNameChange()}
      />
      <input
        type="text"
        id={ids.lastName}
        placeholder="last name filter"
        title="last name filter"
        onChange={() => onLastNameChange()}
      />
      <input
        type="text"
        id={ids.patronymic}
        placeholder="patronymic filter"
        title="patronymic filter"
        onChange={() => onPatronymicChange()}
      />
      <input
        type="text"
        id={ids.position}
        placeholder="position filter"
        title="position filter"
        onChange={() => onPositionChange()}
      />
      <input
        type="text"
        id={ids.ageLess}
        placeholder="age maximum filter"
        title="age maximum filter"
        onChange={() => onAgeChange()}
      />
      <input
        type="text"
        id={ids.ageMore}
        placeholder="age minimum filter"
        title="age minimum filter"
        onChange={() => onAgeChange()}
      />
      <input
        type="text"
        id={ids.experienceLess}
        placeholder="experience maximum filter"
        title="experience maximum filter"
        onChange={() => onExperienceChange()}
      />
      <input
        type="text"
        id={ids.experienceMore}
        placeholder="experience minimum filter"
        title="experience minimum filter"
        onChange={() => onExperienceChange()}
      />
      <input
        type="text"
        id={ids.childrenCountLess}
        placeholder="children count maximum filter"
        title="children count maximum filter"
        onChange={() => onChildrenCountChange()}
      />
      <input
        type="text"
        id={ids.childrenCountMore}
        placeholder="children count minimum filter"
        title="children count minimum filter"
        onChange={() => onChildrenCountChange()}
      />
      <p>
        <input
          type="checkbox"
          id={ids.showDismissed}
          onChange={() => onDismissedChange()}
        />
        Show only dismissed
      </p>
    </div>
  )
}
