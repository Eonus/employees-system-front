import { ApiCallerInterface, RequestMethod } from "../../abstractions/api"
import { EmployeeInterface } from "../../abstractions/employees"
import {
  EmployeeFilter,
  EmployeeRestApiHandlerInterface,
  EmployeeRestObject,
} from "../../abstractions/employees/handler"

export class EmployeeHandler implements EmployeeRestApiHandlerInterface {
  private url: string
  private api: ApiCallerInterface
  constructor(url: string, api: ApiCallerInterface) {
    this.url = url
    this.api = api
  }

  getOne = async (uuid: string): Promise<EmployeeInterface> => {
    const result = await this.api.request(
      this.url + "/" + uuid,
      RequestMethod.Get,
      {},
    )
    return result.element
  }

  getOneByEmail = async (email: string): Promise<EmployeeInterface> => {
    const result = await this.getMany()
    for (let i = 0; i < result.length; i++) {
      if (result[i].contactInfo === email) return result[i]
    }
    throw Error("You are not employee")
  }

  getMany = async (
    filter?: EmployeeFilter | undefined,
  ): Promise<EmployeeInterface[]> => {
    const result = await this.api.request(
      this.url,
      RequestMethod.Get,
      filter || {},
    )
    return result.elements
  }

  post = async (object: Omit<EmployeeInterface, "uuid">): Promise<string> => {
    const result = await this.api.request(this.url, RequestMethod.Post, object)
    return result.objectUuid
  }

  put = async (
    uuid: string,
    object: Partial<EmployeeRestObject>,
  ): Promise<void> => {
    await this.api.request(this.url + "/" + uuid, RequestMethod.Update, object)
  }

  delete = async (uuid: string): Promise<void> => {
    await this.api.request(this.url + "/" + uuid, RequestMethod.Delete, {})
  }
}
