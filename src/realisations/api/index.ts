import axios, { AxiosResponse } from "axios"
import { RequestMethod } from "../../abstractions/api"

export class ApiCaller {
  request = async (
    url: string,
    method: RequestMethod,
    data: { [key: string]: any },
  ) => {
    let result: AxiosResponse<any>
    try {
      result = await this.makeRequest(url, method, data)
    } catch (e) {
      throw Error("Can not make request - " + e.message)
    }
    if (result.status !== 200) {
      throw Error("Request failed with status " + result.status)
    }
    if (result.data.status !== "ok") {
      if (result.data?.error && result.data?.error?.message) {
        throw Error(result.data.error.message)
      } else {
        console.error(result.data)
        throw Error("Status error, no details")
      }
    }
    return result.data
  }

  private makeRequest = async (
    url: string,
    method: RequestMethod,
    data: { [key: string]: any },
  ): Promise<AxiosResponse<any>> => {
    switch (method) {
      case RequestMethod.Post: {
        return await axios.post(url, data)
      }
      case RequestMethod.Update: {
        return await axios.put(url, data)
      }
      case RequestMethod.Delete: {
        return await axios.delete(url, data)
      }
      case RequestMethod.Get: {
        return await axios.get(url, { params: data })
      }
      default:
        throw Error("Such method is not defined")
    }
  }
}
