import { ApiCallerInterface, RequestMethod } from "../../../abstractions/api"
import {
  AuthBaseInterface,
  AuthProviderInterface,
} from "../../../abstractions/api/auth"
import { UserInterface } from "../../../abstractions/users"

export class AuthProvider implements AuthProviderInterface<UserInterface> {
  private loginUrl: string
  private registerUrl: string
  private api: ApiCallerInterface
  constructor(loginUrl: string, registerUrl: string, api: ApiCallerInterface) {
    this.loginUrl = loginUrl
    this.registerUrl = registerUrl
    this.api = api
  }

  register = async (info: UserInterface): Promise<void> => {
    await this.api.request(this.registerUrl, RequestMethod.Post, info)
  }

  login = async (info: AuthBaseInterface): Promise<{ user: UserInterface }> => {
    return await this.api.request(this.loginUrl, RequestMethod.Post, info)
  }
}
