import { EmployeeInterface, Gender } from "../../../abstractions/employees"
import { EmployeeState } from "../../../abstractions/store"
import {
  EmployeeAction,
  EmployeeActionType,
} from "../../../abstractions/store/employee"
import { ReducerInterface } from "../../../abstractions/store/reducer"
import { v4 as uuid } from "uuid"

const defaultEmployee: EmployeeInterface = {
  uuid: "",
  firstName: "",
  lastName: "",
  patronymic: "",
  gender: Gender.Male,
  contactInfo: "",
  salary: 0,
  position: "",
  isCreating: true,
  childrenCount: 0,
  age: 0,
  experience: 0,
}

export class EmployeeReducer
  implements ReducerInterface<EmployeeState, EmployeeAction> {
  protected defaultState: EmployeeState
  constructor(defaultState: EmployeeState) {
    this.defaultState = defaultState
  }

  getElementsWithDelete = (
    state: EmployeeState,
    uuid: string,
  ): EmployeeInterface[] => {
    const result = []
    for (let i = 0; i < state.elements.length; i++) {
      if (state.elements[i].uuid !== uuid) result.push(state.elements[i])
    }
    return result
  }

  getElementsWithFindAndUpdate = (
    state: EmployeeState,
    uuid: string,
    update: Partial<EmployeeInterface>,
  ) => {
    const result = []
    for (let i = 0; i < state.elements.length; i++) {
      result[i] = {
        ...state.elements[i],
      }
      if (state.elements[i].uuid === uuid) {
        result[i] = {
          ...result[i],
          ...update,
        }
      }
    }
    return result
  }

  reduce = (state: EmployeeState, action: EmployeeAction): EmployeeState => {
    switch (action.type) {
      case EmployeeActionType.GetSuccess: {
        return {
          ...state,
          elements: [...action.elements],
        }
      }
      case EmployeeActionType.DeleteSuccess: {
        return {
          ...state,
          elements: this.getElementsWithDelete(state, action.uuid),
        }
      }
      case EmployeeActionType.DeleteError: {
        alert("Delete " + action.uuid + " error - " + action.error)
        return {
          ...state,
        }
      }
      case EmployeeActionType.StartCreating: {
        return {
          ...state,
          elements: [...state.elements, { ...defaultEmployee, uuid: uuid() }],
        }
      }
      case EmployeeActionType.CancelCreating: {
        return {
          ...state,
          elements: this.getElementsWithDelete(state, action.uuid),
        }
      }
      case EmployeeActionType.CreateSuccess: {
        return {
          ...state,
          elements: this.getElementsWithFindAndUpdate(
            state,
            action.element.uuid,
            { ...action.element, uuid: action.elementUuid, isCreating: false },
          ),
        }
      }
      case EmployeeActionType.UpdateSuccess: {
        return {
          ...state,
          elements: this.getElementsWithFindAndUpdate(
            state,
            action.uuid,
            action.elementUpdate,
          ),
        }
      }

      default: {
        if (!state) return { ...this.defaultState }

        return { ...state }
      }
    }
  }
}
