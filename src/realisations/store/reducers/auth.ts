import { AuthState } from "../../../abstractions/store"
import {
  AuthAction,
  AuthActionType,
  LoginErrorAction,
  LoginSuccessAction,
  RegisterSuccessAction,
} from "../../../abstractions/store/auth"
import { ReducerInterface } from "../../../abstractions/store/reducer"

export class AuthReducer implements ReducerInterface<AuthState, AuthAction> {
  protected defaultState: AuthState
  constructor(defaultState: AuthState) {
    this.defaultState = defaultState
  }

  reduce = (state: AuthState, action: AuthAction): AuthState => {
    switch (action.type) {
      case AuthActionType.LoginSuccess: {
        const loginAction = action as LoginSuccessAction
        return {
          logged: true,
          auth: {
            login: loginAction.login,
            password: loginAction.password,
            adminAccess: loginAction.adminAccess,
            email: loginAction.email,
          },
        }
      }
      case AuthActionType.RegisterSuccess: {
        const registerAction = action as RegisterSuccessAction
        return {
          logged: true,
          auth: {
            login: registerAction.login,
            password: registerAction.password,
            email: registerAction.email,
          },
        }
      }
      case AuthActionType.LoginError: {
        const errorAction = action as LoginErrorAction
        return {
          logged: false,
          error: errorAction.error,
        }
      }
      default: {
        if (!state) return { ...this.defaultState }

        return { ...state }
      }
    }
  }
}
