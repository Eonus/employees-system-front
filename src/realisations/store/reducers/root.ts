import { combineReducers } from "@reduxjs/toolkit"
import { AuthState, EmployeeState } from "../../../abstractions/store"
import { AuthReducer } from "./auth"
import { EmployeeReducer } from "./employee"

const defaultAuthState: AuthState = {
  logged: false,
}

const defaultEmployeeState: EmployeeState = {
  elements: [],
}

export const rootReducer = combineReducers({
  auth: new AuthReducer(defaultAuthState).reduce,
  employees: new EmployeeReducer(defaultEmployeeState).reduce,
})
